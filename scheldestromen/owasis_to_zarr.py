# convert the stack of OWASIS GeoTIFF files with timestamp to a single Zarr dataset
# such that it can be processed more easily

from pathlib import Path

import imod
import numpy as np
import xarray as xr
import zarr

datadir = Path()
bb = imod.rasterio.open(datadir / "owasis/owasis/beschikbare_bodemberging/*.tif")
bv = imod.rasterio.open(datadir / "owasis/owasis/bodemvocht/*.tif")
gw = imod.rasterio.open(datadir / "owasis/owasis/grondwaterstand/*.tif")

# these two datasets are encoded as integers
# convert to float32 such that we can use NaN
bb = bb.where(bb != bb.attrs["nodata"]).astype(np.float32)
bv = bv.where(bv != bv.attrs["nodata"]).astype(np.float32)

ds = xr.Dataset(
    {"beschikbare_bodemberging": bb, "bodemvocht": bv, "grondwaterstand": gw}
).unify_chunks()

compressor = zarr.Blosc(cname="zstd", clevel=3, shuffle=2)
encoding = {vname: {"compressor": compressor} for vname in ds.variables}

ds.to_zarr("owasis.zarr", encoding=encoding)
