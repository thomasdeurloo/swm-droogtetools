# read the OWASIS raster Zarr dataset, take the zonal mean in the peilgebieden,
# and save it to a separate, much smaller, table

from pathlib import Path

import geopandas as gpd
import imod
import pandas as pd
import xarray as xr

datadir = Path()
shpdir = datadir / "shp"
gpkgdir = datadir / "gpkg"
featherdir = datadir / "feather"

featherdir.mkdir(parents=True, exist_ok=True)
gpkgdir.mkdir(parents=True, exist_ok=True)

ds = xr.open_zarr(str(datadir / "owasis.zarr"))


def concat_geodataframes(paths):
    gdfs = [gpd.read_file(path) for path in paths]
    return pd.concat(gdfs)


def numeric_part(s):
    # "asda342" -> 342.0
    return float("".join(c for c in s if c.isdigit()))


gdf_gemaal = concat_geodataframes(shpdir.glob("Gema*.shp"))
gdf_gemaal.to_file(gpkgdir / "gemaal.gpkg", driver="GPKG")

gdf_afvoergebied = concat_geodataframes(shpdir.glob("Afvoergebied_*.shp"))
gdf_peilgebied = concat_geodataframes(shpdir.glob("Peilgebieden_*.shp"))
# column GAFIDENT has values like GAF56 for Hogerwaardpolder,
# if we strip "GAF" and turn it into a number, we can use it to group
gdf_afvoergebied["IDENT"] = gdf_afvoergebied["GAFIDENT"].apply(numeric_part)
gdf_peilgebied["IDENT"] = gdf_peilgebied["GPGIDENT"].apply(numeric_part)

# add afvoergebied name and id to peilgebied shape
gdf_peilgebied = gpd.sjoin(
    gdf_peilgebied,
    gdf_afvoergebied[["GAFIDENT", "GAFNAAM", "geometry"]],
    how="left",
    op="intersects",
)

# {5.0: "Dreischor", 56.0: "Hogerwaardpolder", 28.0: "Kleverskerke", 30.0: "Quarles"}
id_map_afvoergebied = gdf_afvoergebied.set_index("IDENT")["GAFNAAM"].to_dict()
id_map_peilgebied = gdf_peilgebied.set_index("IDENT")["GAFNAAM"].to_dict()

ds["peilgebied"] = imod.prepare.rasterize(
    gdf_peilgebied, ds["beschikbare_bodemberging"].isel(time=0), column="IDENT"
)

# take the zonal means
gebied_mean = ds.groupby("peilgebied").mean()
gebied_max = ds.groupby("peilgebied").max()
gebied_min = ds.groupby("peilgebied").min()
datacols = ["beschikbare_bodemberging", "bodemvocht", "grondwaterstand"]
datamap_max = {k: k + "_max" for k in datacols}
datamap_min = {k: k + "_min" for k in datacols}
df_mean = gebied_mean.to_dataframe().reset_index()
df_max = gebied_max.to_dataframe().reset_index()[datacols].rename(columns=datamap_max)
df_min = gebied_min.to_dataframe().reset_index()[datacols].rename(columns=datamap_min)

df = pd.concat([df_mean, df_min, df_max], axis=1)

for idx, row in gdf_gemaal.iterrows():
    ds_1_gemaal = ds.sel(x=row.geometry.x, y=row.geometry.y, method="nearest")
    df_1_gemaal = (
        ds_1_gemaal.to_dataframe()
        .drop(columns=["dx", "dy", "x", "y", "peilgebied"])
        .reset_index()
    )
    gem_path = featherdir / "owasis-on-gemaal-pixel" / (row["KGMNAAM"] + ".feather")
    df_1_gemaal.to_feather(gem_path)
    df_1_gemaal.to_csv(gem_path.with_suffix(".csv"), index=False)


# add the area names and remove unneeded columns
df["afvoergebied"] = df["peilgebied"].replace(id_map_peilgebied)
df = df.drop(columns=["dx", "dy"])
df

# save combined peilgebied file with afvoergebied name to GeoPackage
gdf_peilgebied.to_file(gpkgdir / "peilgebied.gpkg", driver="GPKG")

df.to_feather(featherdir / "owasis-zonal-mean.feather")
df.to_csv(featherdir / "owasis-zonal-mean.csv", index=False)
