# plot processed data

from pathlib import Path

import numpy as np
import pandas as pd
import plotly.express as px

datadir = Path()
xlsxdir = datadir / Path("xlsx")
featherdir = datadir / Path("feather")

df_meteo = pd.read_feather(featherdir / "meteo.feather")
df_stuw = pd.read_feather(featherdir / "stuw-zuiddijk-dreischor.feather")
df_meetpunt = pd.read_feather(featherdir / "meetpunt.feather")
df_gemaal = pd.read_feather(featherdir / "gemaal.feather")


### gemaal


# split it into level and discharge
df_gemaal_lvl = df_gemaal[["time", "wl_in", "wl_out", "afvoergebied"]]
df_gemaal_q = df_gemaal[["time", "q", "afvoergebied"]]

# plot level
df_gemaal_lvl_long = pd.melt(
    df_gemaal_lvl,
    id_vars=["time", "afvoergebied"],
    var_name="parameter",
    value_name="value",
)
fig_gemaal_lvl = px.line(
    df_gemaal_lvl_long,
    x="time",
    y="value",
    color="parameter",
    facet_row="afvoergebied",
    height=1000,
    width=1600,
)
fig_gemaal_lvl.update_yaxes(matches=None)
fig_gemaal_lvl.write_html(str(datadir / "fig" / "gemaal_level.html"))

# plot discharge
fig_gemaal_q = px.area(
    df_gemaal_q, x="time", y="q", facet_row="afvoergebied", height=1000, width=1600
)
fig_gemaal_q.update_yaxes(matches=None)
fig_gemaal_q.write_html(str(datadir / "fig" / "gemaal_q.html"))


### meetpunten


df_meetpunt_long = pd.melt(
    df_meetpunt, id_vars="time", var_name="location", value_name="wl"
)
fig_meetpunt = px.line(
    df_meetpunt_long, x="time", y="wl", color="location", height=1000, width=1600
)
fig_meetpunt.write_html(str(datadir / "fig" / "meetpunt.html"))


### stuw Dreischor


# split it into level and discharge
df_stuw_lvl = df_stuw[["time", "wl_out", "wl_in", "klep"]]
df_stuw_q = df_stuw[["time", "q"]]

df_stuw_lvl_long = pd.melt(
    df_stuw_lvl, id_vars="time", var_name="parameter", value_name="value"
)
fig_stuw_lvl = px.line(
    df_stuw_lvl_long, x="time", y="value", color="parameter", height=1000, width=1600
)
fig_stuw_lvl.write_html(str(datadir / "fig" / "stuw_level.html"))

fig_stuw_q = px.area(df_stuw_q, x="time", y="q", height=1000, width=1600)
fig_stuw_q.write_html(str(datadir / "fig" / "stuw_q.html"))
df_stuw_q


### meteo


df_meteo_long = pd.melt(
    df_meteo, id_vars="time", var_name="parameter", value_name="value"
)
fig_meteo = px.area(
    df_meteo_long, x="time", y="value", facet_row="parameter", height=1000, width=1600
)

df_running = df_meteo.set_index("time")["2020"]
df_running["netprec"] = df_running.prec - df_running.evap
df_running["netprec_cum"] = df_running["netprec"].cumsum()
df_running.plot()
