# plot the water balance data, as well as a simple model of groundwater levels

from pathlib import Path

import matplotlib.pyplot as plt
import pandas as pd

plt.rcParams["figure.figsize"] = [18, 16]
plt.rcParams["legend.fontsize"] = 20
plt.rcParams["legend.handlelength"] = 2

datadir = Path()
xlsxdir = datadir / "xlsx"
featherdir = datadir / "feather"
baldir = datadir / "balance"
plotdir = datadir / "fig" / "bal"
gpkgdir = datadir / "gpkg"

afvoergebieden = [
    "Quarles",
    "Hogerwaardpolder",
    "Kleverskerke",
    "Dreischor",
    "Zonnemaire",
]


def read_balance(afv_sel):
    df = pd.read_feather(baldir / f"balans-{afv_sel}.feather").set_index("time")
    # water balance terms
    df["in_mm"] = df.prec + 0.5  # 0.5 mm/day seepage, fitted to close balance
    # df["fast_storage_mm"] = df.wl_in_mm
    df["out_mm"] = df.q_mm + df.evap
    df["beschikbare_bodemberging_min"] *= -1
    df["beschikbare_bodemberging"] *= -1
    df["beschikbare_bodemberging_max"] *= -1
    df["grondwaterstand_min"] *= 1000
    df["grondwaterstand"] *= 1000
    df["grondwaterstand_max"] *= 1000
    return df


def add_gwl_col(df):
    df["calc_gwl"] = df.wl_in_mm
    prev_gwl = df[year].iloc[0].wl_in_mm
    for idx, row in df[year].iterrows():
        gwl = calc_gwl(prev_gwl, row.prec, row.evap, row.wl_in_mm)
        df.loc[idx, "calc_gwl"] = gwl
        prev_gwl = gwl
    return df


def calc_gwl(prev_gwl, prec, evap, wl):
    recharge = prec - evap + 0.1
    diff_gwl = recharge - 0.15 * (prev_gwl - wl)
    gwl = prev_gwl + diff_gwl
    return gwl


year = "2018"
afvoergebied = "Hogerwaardpolder"
df = read_balance(afvoergebied)

# see if the balance is more or less closed over longer timescales
df.groupby("year").sum().loc[2010:2019, ["in_mm", "out_mm"]].plot()


df.loc[year, :]

df.loc[year, ["in_mm", "out_mm"]].plot()
df.loc[year, ["in_mm", "out_mm"]].cumsum().plot()

# owasis + measured data
df.loc[year, "prec"].plot()
df.loc[year, ["wl_in_mm", "beschikbare_bodemberging", "grondwaterstand"]].plot()

df = add_gwl_col(df)

df.info()
df.loc[
    year,
    ["wl_in_mm", "beschikbare_bodemberging", "grondwaterstand", "calc_gwl", "netprec"],
].plot()


# read the pixel values on top of the gemaal
# Hogerwaardpolder becomes Hogerwaard
pix_path = featherdir / "owasis-on-gemaal-pixel" / f"Gemaal Hogerwaard.feather"
pix_df = (
    pd.read_feather(pix_path)
    .set_index("time")
    .rename(
        columns={
            "beschikbare_bodemberging": "beschikbare_bodemberging_gemaal",
            "grondwaterstand": "grondwaterstand_gemaal",
        }
    )
    .drop(columns="bodemvocht")
)
pix_df["beschikbare_bodemberging_gemaal"] *= -1
pix_df["grondwaterstand_gemaal"] *= 1000

df2 = pd.concat([df, pix_df], axis=1)

# plot spread in OWASIS data
df2[
    [
        "beschikbare_bodemberging_min",
        "beschikbare_bodemberging",
        "beschikbare_bodemberging_max",
        "beschikbare_bodemberging_gemaal",
    ]
].plot()
df2[
    [
        "grondwaterstand_min",
        "grondwaterstand",
        "grondwaterstand_max",
        "grondwaterstand_gemaal",
    ]
].plot()
