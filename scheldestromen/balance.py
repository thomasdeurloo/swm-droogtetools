# combine the different data sources and convert units to form a simple water balance

from pathlib import Path

import geopandas as gpd
import numpy as np
import pandas as pd
import plotly.express as px

datadir = Path()
xlsxdir = datadir / Path("xlsx")
featherdir = datadir / Path("feather")
baldir = datadir / Path("balance")
gpkgdir = datadir / Path("gpkg")

afvoergebieden = [
    "Quarles",
    "Hogerwaardpolder",
    "Kleverskerke",
    "Dreischor",
    "Zonnemaire",
]

time_sel = slice("2010-01-01", "2020-10-02")
time = pd.date_range(time_sel.start, time_sel.stop)

df_meteo = pd.read_feather(featherdir / "meteo.feather")
df_gemaal = pd.read_feather(featherdir / "gemaal.feather")
df_owasis = pd.read_feather(featherdir / "owasis-zonal-mean.feather")
df_meetpunt = pd.read_feather(featherdir / "meetpunt.feather")
# df_stuw = pd.read_feather(featherdir / "stuw-zuiddijk-dreischor.feather")

gdf = gpd.read_file(gpkgdir / "peilgebied.gpkg")
afvoergebied_areas = gdf.groupby("GAFNAAM")["SHAPE_AREA"].sum().to_dict()
afvoergebied_areas["Zonnemaire"] = 488365.0
afvoergebied_areas["Dreischor"] -= 488365.0

meetpunt_map = {
    "Quarles": None,
    "Hogerwaardpolder": None,
    "Kleverskerke": [
        "MPN7516 - Stuw Langeweg, Arnemuiden",
        "MPN7523 - duiker, Doeleweg",
    ],
    "Dreischor": ["MPN7078 - Molenweg, Dreischor"],
    "Zonnemaire": None,
}


def in_time(df):
    "Select desired time range"
    return df.set_index("time").loc[time_sel].reset_index()


def sel_area(df, area):
    "Select desired area"
    return df.loc[df["afvoergebied"] == area].drop(columns=["afvoergebied"])


def net_precip(group):
    "Calculate the cumulative net precipitation"
    group["netprec"] = (group["prec"] - group["evap"]).cumsum()
    # group["qsum"] = group["q"].cumsum()
    return group


def add_time_cols(df):
    df["year"] = df.index.year
    df["dayofyear"] = df.index.dayofyear
    df["month"] = df.index.month

    # add a fake 2020 time for recognizing the time in a plot
    dff = df.reset_index()
    # needs to be a leap year to fit feb 29
    dff["timeofyear"] = pd.to_datetime(
        {"year": 2020, "month": df.index.month, "day": df.index.day}
    )
    df = dff.set_index("time")
    return df


def combine_data(afv_sel, df_meteo, df_gemaal):
    "Make a combined dataframe of different data sources"
    df = (
        df_gemaal.pipe(sel_area, afv_sel)
        .pipe(in_time)
        .set_index("time")
        .loc[:, ["wl_in", "q"]]
    )
    # fill small gaps
    df["wl_in"] = df["wl_in"].interpolate("linear", limit=3)

    to_concat = [df, df_meteo.set_index("time")]
    mp_cols = meetpunt_map[afv_sel]
    if mp_cols is not None:
        meetpnt = in_time(df_meetpunt).set_index("time")[mp_cols]
        renames = {col: col.split()[0] for col in mp_cols}
        meetpnt = meetpnt.rename(columns=renames)
        to_concat.append(meetpnt)

    owasis_sel = (
        df_owasis.pipe(sel_area, afv_sel)
        .pipe(in_time)
        # average over the peilgebieden withing an afvoergebied
        .groupby("time")
        .agg(
            {
                "beschikbare_bodemberging_min": "min",
                "beschikbare_bodemberging": "mean",
                "beschikbare_bodemberging_max": "max",
                "grondwaterstand_min": "min",
                "grondwaterstand": "mean",
                "grondwaterstand_max": "max",
            }
        )
    )
    to_concat.append(owasis_sel)
    return pd.concat(to_concat, axis=1)


for afv_sel in afvoergebieden:
    print(f"{afv_sel=}")
    outdir = baldir
    outdir.mkdir(exist_ok=True)

    df = combine_data(afv_sel, df_meteo, df_gemaal)
    df = add_time_cols(df)

    df = df.groupby("year").apply(net_precip)

    df["wl_in_mm"] = df["wl_in"] * 1000
    area = afvoergebied_areas[afv_sel]
    df["q_mm"] = (df["q"] / area) * 1000

    df.to_excel(outdir / f"balans-{afv_sel}.xlsx")
    df.reset_index().to_feather(outdir / f"balans-{afv_sel}.feather")

    df = df.rename(
        columns={"beschikbare_bodemberging": "b_bodemberging", "grondwaterstand": "gws"}
    )
    df_long = pd.melt(
        df[
            [
                "wl_in",
                "q",
                "prec",
                "evap",
                "netprec",
                "gws",
                "b_bodemberging",
                "year",
                "month",
                "dayofyear",
                "timeofyear",
            ]
        ],
        id_vars=["year", "month", "dayofyear", "timeofyear"],
        var_name="param",
        value_name="value",
    )

    fig = px.line(df_long, x="timeofyear", y="value", color="year", facet_row="param")
    fig.update_yaxes(matches=None)
    fig.write_html(str(outdir / f"timeseries-{afv_sel}.html"))
