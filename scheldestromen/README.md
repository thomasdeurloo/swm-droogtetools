This is the code produced during the analysis of the Slim Water Management
project Droogtetools, case Scheldestromen.

## Preprocessing
owasis_to_zarr.py
    convert the stack of OWASIS GeoTIFF files with timestamp to a single Zarr dataset
    such that it can be processed more easily
    (input not included due to file size, use the processed table in feather/ directly)
owasis_sel.py
    read the OWASIS raster Zarr dataset, take the zonal mean in the peilgebieden,
    and save it to a separate, much smaller, table
    (input not included due to file size, use the processed table in feather/ directly)
excel_read.py
    read data from Excel, resample to daily timeseries, and save to Feather files
    which can be used for faster further processing

## Modeling
balance.py
    combine the different data sources and convert units to form a simple water balance

## Visualisation
owasis_plot.py
    plot the processed OWASIS data per peilgebied
balance_plot.py
    plot the water balance data, as well as a simple model of groundwater levels
caseprep.py
    plot processed data


### Directory structure
Achtergrondinformatie
    background information, not used by the scripts
balance
    water balance outputs are saved here
feather
    intermediate data is saved here as both CSV and Feather files
fig
    plots are saved here
gpkg
    processed (combined) GIS files are saved here in GeoPackage format
shp
    original shapefiles, some of which are further processed
xlsx
    original Excel workbooks with timeseries data

### Required Python (3.8) packages
- matplotlib
- plotly
- geopandas
- numpy
- pandas
