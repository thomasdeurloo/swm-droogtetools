# plot the processed OWASIS data per peilgebied

from pathlib import Path

import pandas as pd
import plotly.express as px

datadir = Path()

df = pd.read_feather(datadir / "feather" / "owasis-zonal-mean.feather")
df["gebied"] = df["afvoergebied"]
df["peilgebied"] = df["peilgebied"].astype(int)
df["peilgeb"] = df["gebied"] + " PG " + df["peilgebied"].astype(str)
df = df.sort_values(by=["afvoergebied", "peilgebied"])

# use plotly to make an interactive plot per sub area
fig_bb = px.line(
    df,
    x="time",
    y="beschikbare_bodemberging",
    color="peilgeb",
    facet_row="gebied",
    height=1000,
    width=1600,
)

fig_gw = px.line(
    df,
    x="time",
    y="grondwaterstand",
    color="peilgeb",
    facet_row="gebied",
    height=1000,
    width=1600,
)

fig_bb.write_html(str(datadir / "fig" / "beschikbare_bodemberging.html"))
fig_gw.write_html(str(datadir / "fig" / "grondwaterstand.html"))
