# read data from Excel, resample to daily timeseries, and save to Feather files
# which can be used for faster further processing

from pathlib import Path

import numpy as np
import pandas as pd

datadir = Path()
xlsxdir = datadir / Path("xlsx")
featherdir = datadir / Path("feather")

gemaal_dreischor_path = xlsxdir / "Gemaal_Dreischor.xlsx"
gemaal_hogerwaard_path = xlsxdir / "Gemaal_Hogerwaard.xlsx"
gemaal_kleverskerke_path = xlsxdir / "Gemaal_Kleverskerke.xlsx"
gemaal_quarles_path = xlsxdir / "Gemaal_Quarles.xlsx"
gemaal_zonnemaire_path = xlsxdir / "Gemaal_Zonnemaire.xlsx"
meetpunten_path = xlsxdir / "Data_meetpunten.xlsx"
meteo_gegevens_path = xlsxdir / "Meteo_gegevens.xlsx"
stuw_dreischor_path = xlsxdir / "Stuw_Zuiddijk_Dreischor.xlsx"


### gemaal


def read_gemaal(path, wl_names=["wl_in"]):
    # read waterlevel, deduplicate and resample to days
    df_wl = pd.read_excel(
        path, 0, skiprows=5, parse_dates=True, header=None, names=wl_names
    )
    df_wl = df_wl[~df_wl.index.duplicated(keep="first")]
    df_wl = df_wl.sort_index()
    df_wl_d = df_wl.resample("D").mean()

    # read discharge of pumps, take total over pumps, resample to days
    df_q = pd.read_excel(path, 1, skiprows=5, parse_dates=True, header=None)
    # sometime parse_dates doesn't make the first column the index
    if df_q.index.dtype == np.int64:
        df_q = df_q.set_index(0)
        df_q.index.name = "time"

    df_q_d = df_q.sum(axis=1).to_frame("q").resample("D").sum()

    # in bedrijf not currently used
    # df_on = pd.read_excel(
    #     path, 2, skiprows=5, parse_dates=True, header=None, names=["on1", "on2"]
    # )
    # df_on = df_on.astype("boolean")  # bool that allows NA

    df = pd.concat([df_wl_d, df_q_d], axis=1)
    df.index.name = "time"
    return df.reset_index()


df_qu = read_gemaal(gemaal_quarles_path)
df_ho = read_gemaal(gemaal_hogerwaard_path)
df_kl = read_gemaal(gemaal_kleverskerke_path)
df_dr = read_gemaal(gemaal_dreischor_path)
df_zo = read_gemaal(gemaal_zonnemaire_path, wl_names=["wl_out", "wl_in"])

# combine the gemalen
df_qu["afvoergebied"] = "Quarles"
df_ho["afvoergebied"] = "Hogerwaardpolder"
df_kl["afvoergebied"] = "Kleverskerke"
df_dr["afvoergebied"] = "Dreischor"
df_zo["afvoergebied"] = "Zonnemaire"

df_gemaal = pd.concat([df_qu, df_ho, df_kl, df_dr, df_zo]).reset_index(drop=True)
df_gemaal.to_feather(featherdir / "gemaal.feather")
df_gemaal.to_csv(featherdir / "gemaal.csv")


### meetpunten


def read_meetpunt():
    mp_names = [
        "MPN7078 - Molenweg, Dreischor",
        "MPN7516 - Stuw Langeweg, Arnemuiden",
        "MPN7523 - duiker, Doeleweg",
        "MPN7541 - Stuw Rijksweg A58, brandweer",
    ]
    df_meetpunt = (
        pd.read_excel(
            meetpunten_path,
            0,
            skiprows=5,
            parse_dates=True,
            header=None,
            names=mp_names,
        )
        .resample("D")
        .mean()
    )
    df_meetpunt.index.name = "time"
    return df_meetpunt.reset_index()


df_meetpunt = read_meetpunt()
# remove extreme outliers
df_meetpunt.loc[
    df_meetpunt["MPN7078 - Molenweg, Dreischor"] > 0, "MPN7078 - Molenweg, Dreischor"
] = np.nan
df_meetpunt.to_feather(featherdir / "meetpunt.feather")
df_meetpunt.to_csv(featherdir / "meetpunt.csv")


### stuw Dreischor


def read_stuw_dreischor(path):
    wl_names = ["wl_out", "wl_in"]
    df_wl = pd.read_excel(
        path, 0, skiprows=5, parse_dates=True, header=None, names=wl_names
    )
    df_wl = df_wl[~df_wl.index.duplicated(keep="first")]
    df_wl = df_wl.sort_index()
    df_wl_d = df_wl.resample("D").mean()

    # read discharge of pumps, take total over pumps, resample to days
    df_q = pd.read_excel(path, 1, skiprows=5, parse_dates=True, header=None)
    # sometime parse_dates doesn't make the first column the index
    if df_q.index.dtype == np.int64:
        df_q = df_q.set_index(0)
        df_q.index.name = "time"

    df_q_d = df_q.sum(axis=1).to_frame("q").resample("D").sum()

    df_klep = (
        pd.read_excel(
            path, 2, skiprows=5, parse_dates=True, header=None, names=["klep"]
        )
        .resample("D")
        .mean()
    )

    df_stuw = pd.concat([df_wl_d, df_q_d, df_klep], axis=1)
    df_stuw.index.name = "time"
    df_stuw = df_stuw.reset_index()

    return df_stuw


df_stuw = read_stuw_dreischor(stuw_dreischor_path)
df_stuw.to_feather(featherdir / "stuw-zuiddijk-dreischor.feather")
df_stuw.to_csv(featherdir / "stuw-zuiddijk-dreischor.csv")


### meteo


def read_meteo(path):
    meteo_names = [
        "MPN9247 - neerslagstation Vlissingen",
        "MPN9247 - neerslagstation Vlissingen",
        "GAF28 - Kleverskerke",
        "GAF30 - Quarles",
        "GAF5 - Dreischor",
        "GAF56 - Hogerwaardpolder",
    ]
    # read only Vlissingen precipitation and evaporation for now
    df = pd.read_excel(
        path, 0, skiprows=6, parse_dates=True, header=None, usecols="A:C"
    )
    df.columns = ["time", "evap", "prec"]
    df_meteo = df.set_index("time").resample("D").first().reset_index()
    return df_meteo


df_meteo = read_meteo(meteo_gegevens_path)
df_meteo.to_feather(featherdir / "meteo.feather")
df_meteo.to_csv(featherdir / "meteo.csv")
