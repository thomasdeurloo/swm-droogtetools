import logging
from dataclasses import dataclass, field
from typing import List, Dict, Any, Hashable, Optional, Tuple, Callable

import numpy as np
import pandas as pd
import pymc3 as pm
from pymc3.backends.base import MultiTrace

LOGGER = logging.getLogger(__name__)


@dataclass
class Scaler:
    mu: float = None
    sigma: float = None

    def fit(self, x):
        self.mu = x.mean()
        self.sigma = x.std()
        return self

    def transform(self, x):
        return (x - self.mu) / self.sigma

    def fit_transform(self, x):
        self.fit(x)
        return self.transform(x)

    def inverse_transform(self, x):
        return x * self.sigma + self.mu


@dataclass
class PredictiveModel:
    model_factory: Callable[[pd.DataFrame, pd.Series, Optional[Tuple[int]]], pm.Model]
    scale_x_features: List[str] = field(default_factory=list)
    group_features: List[str] = field(default_factory=list)
    scale_y: bool = False
    scalers: Dict[Optional[Hashable], Scaler] = field(init=False, default_factory=dict)
    group_lookups: Dict[Optional[Hashable], Dict[Optional[Hashable], int]] = field(init=False, default_factory=dict)
    group_inv_lookups: Dict[Optional[Hashable], Dict[int, Optional[Hashable]]] = field(init=False, default_factory=dict)
    group_dims: Tuple[int] = field(init=False, default_factory=tuple)
    pymc3_model: pm.Model = field(init=False, default=None)
    posterior_trace: Dict[str, MultiTrace] = field(init=False, default_factory=dict)

    def _init_scalers(self, X: pd.DataFrame, y: pd.Series):
        self.scalers = {
            feature: Scaler().fit(X[feature])
            for feature in self.scale_x_features
        }

        if self.scale_y and y is not None:
            self.scalers.update({y.name: Scaler().fit(y)})

    def _init_groups(self, X: pd.DataFrame):
        self.group_lookups = {
            feature: dict((value, index) for (index, value) in enumerate(X[feature].unique()))
            for feature in self.group_features
        }
        self.group_inv_lookups = {
            feature: dict(enumerate(X[feature].unique()))
            for feature in self.group_features
        }

        self.group_dims = tuple(len(self.group_lookups[f]) for f in self.group_features)

    def _transform_input(self, X: pd.DataFrame, y: pd.Series = None) -> Tuple[pd.DataFrame, pd.Series]:
        return (
            (
                X
                .assign(**{
                    feature: self.scalers[feature].transform(X[feature])
                    for feature in self.scale_x_features
                })
                .assign(**{
                    feature: X[feature].map(self.group_lookups[feature])
                    for feature in self.group_features
                })
            ),
            self.scalers[y.name].transform(y) if (y is not None and self.scale_y) else y
        )

    def fit(self, X: pd.DataFrame, y: pd.Series, **sampler_args) -> 'PredictiveModel':
        self._init_scalers(X, y)
        self._init_groups(X)

        with self.model_factory(*self._transform_input(X, y), self.group_dims) as model:
            self.pymc3_model = model
            self.posterior_trace = pm.sample(return_inferencedata=False, **sampler_args)

        return self

    def prior_predictive(self, X: pd.DataFrame, **sampler_args) -> Dict[Any, np.ndarray]:
        # Call this with the same X as fit()
        if not self.posterior_trace:
            self._init_scalers(X, None)
            self._init_groups(X)

        with self.model_factory(self._transform_input(X)[0], pd.Series(np.zeros(len(X))), self.group_dims) as model:
            if not self.posterior_trace:
                self.pymc3_model = model
            return pm.sample_prior_predictive(**sampler_args)

    def predict(self, X: pd.DataFrame, **sampler_args) -> Dict[Any, np.ndarray]:
        if not self.posterior_trace:
            LOGGER.error('Model has not been fit')

        with self.model_factory(self._transform_input(X)[0], pd.Series(np.zeros(len(X))), self.group_dims) as model:
            return pm.sample_posterior_predictive(self.posterior_trace, **sampler_args)
