import theano.tensor as tt
import numpy as np
from pymc3 import Continuous, Gamma, Bernoulli
from pymc3.distributions import draw_values, generate_samples


class GammaOrZero(Continuous):
    def __init__(self, alpha, beta, p_zero, *args, **kwargs):
        super(GammaOrZero, self).__init__(*args, **kwargs)
        self.alpha = alpha
        self.beta = beta
        self.p_zero = tt.as_tensor_variable(p_zero)
        self.gamma = Gamma.dist(alpha, beta)
        self.bernoulli = Bernoulli.dist(p_zero)

    def logp(self, value):
        return tt.switch(
            value > 0,
            tt.log(1 - self.p_zero) + self.gamma.logp(value),
            tt.log(self.p_zero)
        )

    def _random(self, p_zero, alpha, beta, size=None):
        rng = np.random.default_rng()
        y = rng.gamma(shape=alpha, scale=1/beta, size=size)
        z = rng.binomial(n=1, p=p_zero, size=size)
        y[z == 1] = 0
        return y

    def random(self, point=None, size=None):
        alpha, beta, p_zero = draw_values([self.alpha, self.beta, self.p_zero], point=point, size=size)
        return generate_samples(
            self._random, p_zero=p_zero, alpha=alpha, beta=beta, size=size
        )
