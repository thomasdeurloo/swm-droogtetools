import os
from setuptools import setup, find_packages

setup (
    name='wsbd_util',
    description='utility Python code for WSBD POC project',
    packages=find_packages(exclude=['csv', 'raw', 'shp'])
)
